# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the enigma.hummlbach package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: enigma.hummlbach\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-10-19 14:49+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/ui/delegates/KeyListItem.qml:19
msgid "Remove Key"
msgstr ""

#: ../qml/ui/components/ErrorPopup.qml:12
msgid "Argl!"
msgstr ""

#: ../qml/ui/components/ConfirmationPopup.qml:14
#: ../qml/ui/pages/ImportExportPage.qml:43
msgid "Okay"
msgstr ""

#: ../qml/ui/components/ConfirmationPopup.qml:15
#: ../qml/ui/components/AddKeyDialog.qml:49
#: ../qml/ui/pages/ImportExportPage.qml:30
msgid "Cancel"
msgstr ""

#: ../qml/ui/components/AddKeyDialog.qml:19
msgid "Add Key"
msgstr ""

#: ../qml/ui/components/AddKeyDialog.qml:19
msgid "Adding Key"
msgstr ""

#: ../qml/ui/components/AddKeyDialog.qml:19
msgid "Added Key"
msgstr ""

#: ../qml/ui/components/AddKeyDialog.qml:24
msgid "Enter Email address"
msgstr ""

#: ../qml/ui/components/AddKeyDialog.qml:31
msgid "Enter a passphrase"
msgstr ""

#: ../qml/ui/components/AddKeyDialog.qml:38
msgid "Please repeat the passphrase"
msgstr ""

#: ../qml/ui/components/AddKeyDialog.qml:56
msgid "Create Key"
msgstr ""

#: ../qml/ui/components/AddKeyDialog.qml:75
msgid ""
"Collecting randomness to generate the key, please contribute some entropy by "
"playing around with the indicators for example..."
msgstr ""

#: ../qml/ui/components/AddKeyDialog.qml:90
msgid "Key for "
msgstr ""

#: ../qml/ui/components/AddKeyDialog.qml:90
msgid " was successfully generated and got the fingerprint "
msgstr ""

#: ../qml/ui/components/AddKeyDialog.qml:98
msgid "Close"
msgstr ""

#: ../qml/ui/pages/ImportExportPage.qml:24 ../qml/ui/pages/KeyList.qml:15
#: Enigma.desktop.in.h:1
msgid "Enigma"
msgstr ""

#: ../qml/ui/pages/ImportExportPage.qml:86
msgid "Decrypt"
msgstr ""

#: ../qml/ui/pages/ImportExportPage.qml:87
msgid "Select key for encryption"
msgstr ""

#: ../qml/ui/pages/ImportExportPage.qml:88
msgid "Import key"
msgstr ""

#: ../qml/ui/pages/ImportExportPage.qml:119
msgid "Passphrase for private key"
msgstr ""

#: ../qml/ui/pages/ImportExportPage.qml:225
msgid "Transfer encrypted to..."
msgstr ""

#: ../qml/ui/pages/KeyList.qml:21
msgid "Add key"
msgstr ""

#: ../qml/ui/pages/KeyList.qml:49
msgid "Do you really want to delete the key for "
msgstr ""

#: ../qml/ui/pages/KeyList.qml:49
msgid "?"
msgstr ""

#: ../qml/ui/pages/KeyList.qml:87
msgid "Start by adding PGP keys"
msgstr ""

#: ../qml/ui/pages/KeyList.qml:93
msgid ""
"Click the '+' at the top to generate a new key or\n"
" import your keyring by sharing an asc file to Enigma."
msgstr ""
